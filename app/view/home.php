<div id="welcome">
	<div class="row">
		<div class="wlcTop clearfix">
			<div class="container fl">
				<h2>27 YEARS <span>of Providing Custom Install,<br>Renovation and Irrigation</span> </h2>
				<p>At Cano Custom Landscapes we offer a wide variety of services for our customers</p>
				<p>CALL US TODAY! <span><?php $this->info(["phone","tel"]);?></span> </p>
			</div>
		</div>
		<div class="wlcMid clearfix">
			<img src="public/images/common/house.jpg" alt="house" class="house">
			<div class="wlcMidLeft col-6 fl">
				<h2>WELCOME!</h2>
				<h1>Cano Custom Landscapes</h1>
			</div>
			<div class="wlcMidRight col-6 fl">
				<p>Whether You need something installed, built, repaired, renovated or maintained.</p>
				<p>We hope you can find everything you need. Cano Custom Landscapes is focused on providing high-quality service and customer satisfaction – we will do everything we can to meet your expectations. With a variety of offerings to choose from, we’re sure you’ll be happy working with us. Look around our website and if you have any comments or questions, please feel free to contact us</p>
			</div>
		</div>
		<div class="wlcBot clearfix">
			<dl>
				<dt> <img src="public/images/content/welcome1.jpg" alt="CUSTOM INSTALL & RENOVATION"></dt>
				<dd>CUSTOM INSTALL & RENOVATION</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/welcome2.jpg" alt="IRRIGATION"></dt>
				<dd>IRRIGATION</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/welcome3.jpg" alt="ROCK WALL & PAVE STONE"></dt>
				<dd>ROCK WALL & PAVE STONE</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section2">
	<div class="row">
		<p>At Cano Custom Landscapes, we believe that Only Quality Will Stand the Test of Time. Contact us today to get the best of service!</p>
		<a href="contact#content" class="btn">LEARN MORE</a>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h2>OUR <span>GALLERY</span> </h2>
		<p>At Cano Custom Landscapes, we work closely with local industry professional product suppliers to include C & C Sand and Stone Co.,</p>
		<p>Ewing irrigation, Rocky Top Resources, Don’s Garden Center, Rick’s Garden Center, Templeton Gap Turf Farm, and many more, to provide you the customer with the highest quality materials at the best prices.</p>
	</div>
	<div class="container">
		<div class="home-gallery">
			<div> <img src="public/images/content/gallery/1.jpg" alt="gallery 1"> </div>
			<div> <img src="public/images/content/gallery/2.jpg" alt="gallery 2"> </div>
			<div> <img src="public/images/content/gallery/3.jpg" alt="gallery 3"> </div>
			<div> <img src="public/images/content/gallery/4.jpg" alt="gallery 4"> </div>
			<div> <img src="public/images/content/gallery/5.jpg" alt="gallery 5"> </div>
			<div> <img src="public/images/content/gallery/6.jpg" alt="gallery 6"> </div>
			<div> <img src="public/images/content/gallery/7.jpg" alt="gallery 7"> </div>
			<div> <img src="public/images/content/gallery/8.jpg" alt="gallery 8"> </div>
			<div> <img src="public/images/content/gallery/9.jpg" alt="gallery 9"> </div>
			<div> <img src="public/images/content/gallery/10.jpg" alt="gallery 10"> </div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>CONTACT <span>US</span> </h2>
		<p>Get started with Cano Custom Landscapes today! Our trained staff will be more than happy to provide you with answers and solutions to your needs! We want to hear from you, so call us today at 719-761-0653 or fill out our online form.</p>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="cntLeft col-6 fl">
				<div class="cntLLeft col-6 fl">
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="name:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="phone:">
					</label>
				</div>
				<div class="cntLRight col-6 fl">
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="email:">
					</label>
					<label><span class="ctc-hide">Subject</span>
						<input type="text" name="subject" placeholder="subject:">
					</label>
				</div>
				<div class="clearfix"></div>
				<label><span class="ctc-hide">Message</span>
					<textarea name="comments" cols="30" rows="10" placeholder="comment:"></textarea>
				</label>
			</div>
			<div class="cntRight col-5 fl">
				<br>
				<label>
					<input type="checkbox" name="consent" class="consentBox"><span class="response">I hereby consent to having this website store my<br> submitted information so that they can respond to my inquiry.</span>
				</label><br><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/><span class="response"> I hereby confirm that I have read and understood <br> this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></span>
				</label>
				<?php endif ?>
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>

				<button type="submit" class="ctcBtn btn" disabled>Submit</button>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3106.197377409097!2d-104.82956988465028!3d38.87372577957431!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87134ff22f834749%3A0x42546a731fd48457!2s2906+Beacon+St+Suite+A%2C+Colorado+Springs%2C+CO+80907%2C+USA!5e0!3m2!1sen!2sph!4v1540174669177" class="map" allowfullscreen></iframe>
