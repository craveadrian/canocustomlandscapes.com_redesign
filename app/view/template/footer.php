<footer>
	<div id="footer">
		<div class="footnav">
			<ul>
				 <li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
				 <li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
				 <li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
				 <li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews">REVIEWS</a></li>
				 <li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
				 <li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy">PRIVACY POLICY</a></li>
			</ul>
		</div>
		<div class="footBot">
			<div class="container">
				<div class="row">
					<dl>
							<dt> <img src="public/images/common/sprite.png" alt="address" class="bg-address"> </dt>
							<dd><p><?php $this->info("address"); ?></p></dd>
					</dl>
					<dl>
							<dt><a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" alt="Main Logo" class="hdLogo"> </a></dt>
							<dd>
								<p><?php $this->info(["phone","tel"]); ?></p>
								<p>Custom Intalls, Renovations & Irrigation</p>
							</dd>
					</dl>
					<dl>
							<dt> <img src="public/images/common/sprite.png" alt="email" class="bg-email"> </dt>
							<dd><p><?php $this->info(["email","mailto"]); ?></p></dd>
					</dl>
				</div>
			</div>
		</div>
		<p class="copy">
			© <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved.
			<?php if( $this->siteInfo['policy_link'] ): ?>
				<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
			<?php endif ?>
		</p>
		<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
		<p class="social-m">
			<a href="<?php $this->info("fb_link"); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
			<a href="<?php $this->info("tt_link"); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
			<a href="<?php $this->info("fl_link"); ?>" target="_blank"><i class="fa fa-flickr"></i></a>
			<a href="<?php $this->info("li_link"); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
		</p>
	</div>
</footer>
<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>
<script src="<?php echo URL; ?>public/scripts/aos.js"></script>
<script src="<?php echo URL; ?>public/slick/slick.js"></script>
<script>
	window.onscroll = function() {myFunction()};

	var navbar = document.getElementById("navbar");
	var sticky = navbar.offsetTop;

	function myFunction() {
	  if (window.pageYOffset >= sticky) {
	    navbar.classList.add("sticky")
	  } else {
	    navbar.classList.remove("sticky");
	  }
	}
</script>
<script>
	$(document).ready(function(){
		$('.home-gallery').slick({
			arrows:true,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  variableWidth: true,
			responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 10,
		        slidesToScroll: 10,
		        infinite: true,
		        dots: true
		      }
		    },
				{
		      breakpoint: 800,
		      settings: {
		        arrows:false,
						infinite: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
						arrows:false,
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
						arrows:false,
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	});
</script>
<script>
	AOS.init();
</script>
<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
				$( '.destroy-on-load' ).remove();
			})
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span class="ctc-hide">Call To Action Button</span></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
