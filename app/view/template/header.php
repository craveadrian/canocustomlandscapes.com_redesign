<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="apple-mobile-web-app-status-bar-style" content="black" />
      <?php $this->helpers->seo($view); ?>
      <link rel="icon" href="public/images/favicon.png" type="image/x-icon">
      <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
      <link rel="stylesheet" href="<?php echo URL; ?>public/styles/aos.css" />
      <link rel="stylesheet" href="<?php echo URL; ?>public/slick/slick.css" />
      <link rel="stylesheet" href="<?php echo URL; ?>public/slick/slick-theme.css" />
			<!-- Add the slick-theme.css if you want default styling -->
      <link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
      <link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <?php $this->helpers->analytics(); ?>
   </head>
   <body <?php $this->helpers->bodyClasses($view); ?>>
      <?php $this->checkSuspensionHeader(); ?>
      <header>
         <div id="header">
            <div class="hdTop">
               <div class="container">
								 <div class="row">
									 <dl>
                      <dt> <img src="public/images/common/sprite.png" alt="address" class="bg-address"> </dt>
                      <dd><p><?php $this->info("address"); ?></p></dd>
                   </dl>
                   <dl>
                      <dt><a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" alt="Main Logo" class="hdLogo"> </a></dt>
                      <dd>
												<p><?php $this->info(["phone","tel"]); ?></p>
												<p>Custom Intalls, Renovations & Irrigation</p>
											</dd>
                   </dl>
                   <dl>
                      <dt> <img src="public/images/common/sprite.png" alt="email" class="bg-email"> </dt>
                      <dd><p><?php $this->info(["email","mailto"]); ?></p></dd>
                   </dl>
								 </div>
               </div>
            </div>
            <div id="navbar" class="hdBot">
							<div class="row">
								<nav>
                   <a href="#" id="pull"><strong>MENU</strong></a>
                   <ul>
                      <li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
                      <li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
                      <li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
                      <li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews">REVIEWS</a></li>
                      <li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
                      <li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy">PRIVACY POLICY</a></li>
                   </ul>
                </nav>
							</div>
            </div>
         </div>
      </header>
      <!-- <?php if($view == "home"):?>
         <div id="banner">
         	<div class="row">
         		<h1>BANNER</h1>
         	</div>
         </div>
         <?php endif; ?> -->
