<div id="content">
	<div class="row">
		<h1>Services</h1>
		<div class="container">
			<br>

			<p>At Cano Custom Landscapes we offer a wide variety of services for our customers to include:</p>
			<br>

			<ol>
		<li>Custom landscape installations and renovations;irrigation installation</li>
		<li>Maintenance and repair</li>
		<li>A wide variety of retaining wall applications</li>
		<li>Flagstone and paver patios; property maintenance</li>
		<li>Low voltage lighting features; snow removal in the winter</li>
		<li>Water features and fountains in the spring/summer</li>
		<li>Multiple types of fencing; xeriscape; basic design</li>
		<li>Overall property consultation</li>
			</ol>
			<br>

			<h3 id="custom">Custom Installs &amp; Renovations</h3>
			<p>From a new build, to a complete overhaul of your existing landscape, the most basic of design to projects including outdoor kitchens and fire-pits, Cano Custom Landscapes can build your outdoor living space to suit your desires.</p>
			<br>

			<h3 id="irrigation">Irrigation</h3>
			<p>At Cano Custom Landscapes, while we prefer to use water saving irrigation technology such as MP Rotator low volume irrigation nozzles in our system designs, we are well versed in all other irrigation technologies. Utilizing the MP Rotator, we are able to reduce system water usage by up to 30% and decrease run-off.</p>
			<br>

			<h3 id="stone">Stone, Veneer, Pavers, Walls</h3>
			<p>At Cano Custom Landscapes, we prefer to use quality products chosen for their durability and attractiveness to include; Borgert, Pavestone, Keystone, El Dorado, siloam, flagstone. We install all forms of retaining walls including; boulder, stacked, CMU-veneer, SRW (segmental retaining wall systems), and timber walls.</p>
		</div>
	</div>
</div>
